package com.cb.mathematics.test;

import org.junit.Test;

import junit.framework.TestCase;

import com.cb.mathematics.Arithematic;

public class ArithematicTest3 extends TestCase {

	Arithematic arth = new Arithematic();
	
	
	@Test
	public void testMultiplication() {
		arth.setFirstNum(10);
		arth.setSecondNum(15);
		arth.multiply();
		assertEquals(150, arth.result());
	}

	public void testFailed(){
		assertEquals(10,25);
	}
}
