package com.cb.mathematics.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ArithematicTest3.class,NightlySuite.class})
public class WeeklySuite {

}
