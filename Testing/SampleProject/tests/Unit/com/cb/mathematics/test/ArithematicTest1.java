package com.cb.mathematics.test;

import org.junit.Test;

import junit.framework.TestCase;

import com.cb.mathematics.Arithematic;

public class ArithematicTest1 extends TestCase {

	Arithematic arth = new Arithematic();

	@Test
	public void testAddition() {
		arth.setFirstNum(10);
		arth.setSecondNum(15);
		arth.sum();
		assertEquals(25, arth.result());
//		assertEquals(25, 12);
	}
	
	@Test
	public void testAddition1() {
		arth.setFirstNum(10);
		arth.setSecondNum(15);
		arth.sum();
		assertEquals(25, arth.result());
	}
	
	@Test
	public void testAddition2() {
		arth.setFirstNum(10);
		arth.setSecondNum(15);
		arth.sum();
		assertEquals(25, arth.result());
	}
	
	@Test
	public void testAddition3() {
		arth.setFirstNum(10);
		arth.setSecondNum(15);
		arth.sum();
		assertEquals(25, arth.result());
	}

	@Test
	public void testMultiply() {
		arth.setFirstNum(10);
		arth.setSecondNum(15);
		arth.multiply();
		assertEquals(150, arth.result());
	}

	@Test
	public void testNewMultiply() {
		arth.setFirstNum(10);
		arth.setSecondNum(15);
		arth.multiply();
		assertEquals(150, arth.result());
	}
	
}
