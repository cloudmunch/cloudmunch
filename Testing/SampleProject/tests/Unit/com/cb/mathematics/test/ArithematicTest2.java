package com.cb.mathematics.test;

import org.junit.Test;

import junit.framework.TestCase;

import com.cb.mathematics.Arithematic;

public class ArithematicTest2 extends TestCase {

	Arithematic arth = new Arithematic();
	@Test
	public void testAdditionOfNegNum() {
		arth.setFirstNum(-10);
		arth.setSecondNum(-15);
		arth.sum();
		assertEquals(-25, arth.result());
	}
	
}
