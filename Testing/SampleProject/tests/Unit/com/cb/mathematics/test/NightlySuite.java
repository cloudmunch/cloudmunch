package com.cb.mathematics.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ArithematicTest2.class,QuickSuite.class})
public class NightlySuite {

}
