package com.cb.fixtureSet;

import com.cb.mathematics.Arithematic;

import fitlibrary.DoFixture;


public class ArthimaticFixture extends DoFixture {

	Arithematic arth = new Arithematic();
	
	public void setFirstNumberAs(int n1){
		arth.setFirstNum(n1);
	}
	
	public void setSecondNumberAs(int n1){
		arth.setSecondNum(n1);
	}
	
	public void addBothNumbers(){
		arth.sum();
	}
	
	public int resultOfSumOperationIs(){
		return arth.result();
	}
}
