package com.cb.mathematics.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({Selenium_ArithematicTest2.class,SeleniumQuickSuite.class})
public class SeleniumNightlySuite {

}
