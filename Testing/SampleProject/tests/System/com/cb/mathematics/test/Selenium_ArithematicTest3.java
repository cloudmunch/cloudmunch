package com.cb.mathematics.test;

import org.junit.Test;

import junit.framework.TestCase;

import com.cb.mathematics.Arithematic;

public class Selenium_ArithematicTest3 extends TestCase {

	Arithematic arth = new Arithematic();
	
	
	@Test
	public void testSeleniumMultiplication() {
		arth.setFirstNum(10);
		arth.setSecondNum(17);
		arth.multiply();
		assertEquals(170, arth.result());
	}

}
