package com.cb.mathematics.test;

import org.junit.Test;

import junit.framework.TestCase;

import com.cb.mathematics.Arithematic;

public class Selenium_ArithematicTest2 extends TestCase {

	Arithematic arth = new Arithematic();
	@Test
	public void testSeleniumAdditionOfNegNum() {
		arth.setFirstNum(-10);
		arth.setSecondNum(-1);
		arth.sum();
		assertEquals(-11, arth.result());
	}
	
}
