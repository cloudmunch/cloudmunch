package com.cb.mathematics.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({Selenium_ArithematicTest1.class, Selenium_WebPage_Test.class})
public class SeleniumQuickSuite{

}
