package com.cb.mathematics.test;

import junit.framework.TestCase;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;

public class Selenium_WebPage_Test extends TestCase {

	
	@Test
	public void testGoogleTitle() {
		HtmlUnitDriver driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_3_6);
		driver.get("http://www.google.com/");
		WebElement element = driver.findElement(By.name("q"));
		element.sendKeys("Cheese!");
		element.submit();
		System.out.println("Page title is: " + driver.getTitle());
		assertEquals("Cheese! - Google Search", driver.getTitle());
		driver.quit();
	}
	
	@Test
	public void testGetTitleOfJenkinsProject() {
		HtmlUnitDriver driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_3_6);
		driver.get("http://ec2-107-20-169-27.compute-1.amazonaws.com:8280/jenkins/job/Sample%20Project%20-%20Full/");
//        WebElement element = driver.findElement(By.name("q"));
		System.out.println("Page title is: " + driver.getTitle());
		assertEquals("Sample Project - Full [Jenkins]", driver.getTitle());
		driver.quit();
	}
	
	@Test
	public void testFindCloudboxOnlineWebPage() {
		HtmlUnitDriver driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_3_6);
//		driver.get("http://ec2-107-20-82-193.compute-1.amazonaws.com:8080/cb/cbdashboard.htm");
		driver.get("http://ec2-107-22-213-173.compute-1.amazonaws.com:8080/agilefant");
//        WebElement element = driver.findElement(By.linkText("SoberIT"));
		System.out.println("Page title is: " + driver.getTitle());
//		System.out.println(element.getText());
		assertEquals("Agilefant", driver.getTitle());
		driver.quit();
		
	}
}
