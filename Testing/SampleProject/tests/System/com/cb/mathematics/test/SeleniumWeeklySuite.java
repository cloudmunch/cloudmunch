package com.cb.mathematics.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({Selenium_ArithematicTest3.class,SeleniumNightlySuite.class})
public class SeleniumWeeklySuite {

}
